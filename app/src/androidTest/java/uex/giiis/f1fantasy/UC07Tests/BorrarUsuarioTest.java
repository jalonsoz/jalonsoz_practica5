package uex.giiis.f1fantasy.UC07Tests;

import androidx.test.InstrumentationRegistry;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import uex.giiis.f1fantasy.MainActivity;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.ui.LoginActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class BorrarUsuarioTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void shouldDeleteUser() {

        // Se registra el usuario
        onView(withId(R.id.boton_registro_login)).perform(click());
        onView(withId(R.id.usuario_registrar)).perform(typeText("UserPrueba"));
        onView(withId(R.id.password_registrar)).perform(typeText("1234"));
        onView(withId(R.id.password_confirmar_registrar)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_registro_online)).perform(click());

        // Esperamos a que acabe la Splash Activity
        boolean salir = false;
        while (!salir) {
            try {
                onView(withId(R.id.navigation_calendario)).check(matches(isDisplayed()));
                salir = true;
            } catch (NoMatchingViewException e) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }

        // Borramos al usuario
        openContextualActionModeOverflowMenu();
        onView(withText(R.string.perfil)).perform(click());
        onView(withText(R.string.borrar_perfil)).perform(click());

        // Hacemos login con el usuario borrado para comprobar que no existe
        onView(withId(R.id.usuario)).perform(typeText("UserPrueba"));
        onView(withId(R.id.password)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_inicio_login)).perform(click());
        // Texto indicativo de que no se ha podido hacer login
        onView(withId(R.id.errorLogin)).check(matches(withText(R.string.login_failed)));
    }
}

