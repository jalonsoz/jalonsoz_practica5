package uex.giiis.f1fantasy.network;


import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.ConstructorStandingsList;
import uex.giiis.f1fantasy.generatedPojos.ConstructorTable;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.generatedPojos.StandingsList;
import uex.giiis.f1fantasy.pojo.Information;

public interface onStandingListListener {

    public void onStandingListLoaded(StandingsList standingsList);
    public void onConstructorStandingsList (ConstructorStandingsList constructorStandingsList);
    public void onListDriversConstructors (List<ConstructorTable> listDriversConstructors);
    public void onListRaceResults (List<Race> results);
    public void onListRace (List<Race> races);
    public void onInformation (Information information);
}
