
package uex.giiis.f1fantasy.generatedPojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConstructorTable {

    @SerializedName("driverId")
    @Expose
    private String driverId;
    @SerializedName("Constructors")
    @Expose
    private List<Constructor> constructors = null;

    @JsonProperty("driverId")
    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    @JsonProperty("Constructors")
    public List<Constructor> getConstructors() {
        return constructors;
    }

    public void setConstructors(List<Constructor> constructors) {
        this.constructors = constructors;
    }

}
