package uex.giiis.f1fantasy.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.Observer;

import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStandingsList;
import uex.giiis.f1fantasy.generatedPojos.ConstructorTable;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.generatedPojos.Result;
import uex.giiis.f1fantasy.generatedPojos.StandingsList;
import uex.giiis.f1fantasy.network.F1NetworkDataSource;
import uex.giiis.f1fantasy.pojo.ConstructorComplete;
import uex.giiis.f1fantasy.pojo.ConstructorStandingComplete;
import uex.giiis.f1fantasy.pojo.DriverComplete;
import uex.giiis.f1fantasy.pojo.DriverStandingComplete;
import uex.giiis.f1fantasy.pojo.IdPilotoIdLiga;
import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.pojo.UserLeaguesComplete;


/**
 */
public class F1Repository {
    private static final String LOG_TAG = F1Repository.class.getSimpleName();

    // For Singleton instantiation
    private static F1Repository sInstance;

    private final DaoCollection mDaoCollection;
    private final F1NetworkDataSource mF1NetworkDataSource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final MutableLiveData<Integer> roundLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> usernameLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> driverIdLiveData = new MutableLiveData<>();
    private final MutableLiveData<Integer> idLiveData = new MutableLiveData<>();
    private final MutableLiveData<Integer> idLigaLiveData = new MutableLiveData<>();
    private final MutableLiveData<IdPilotoIdLiga> idPilotoIdLigaLiveData = new MutableLiveData<>();

    private final Map<String, Long> lastUpdateTimeMillisMap = new HashMap<>();

    SharedPreferences sharedPreferences;
    SharedPreferences sharedPreferencesUser;
    private int gpID;

    private F1Repository(DaoCollection daoCollection, F1NetworkDataSource f1NetworkDataSource) {
        mDaoCollection = daoCollection;
        mF1NetworkDataSource = f1NetworkDataSource;
        Context context = mDaoCollection.mContext;
        sharedPreferences = context.getSharedPreferences("ultimaModificacion", context.MODE_PRIVATE);
        sharedPreferencesUser = context.getSharedPreferences("usuario", context.MODE_PRIVATE);

        LiveData<StandingsList> networkDataSL = mF1NetworkDataSource.getmStandingList();
        networkDataSL.observeForever(new Observer<StandingsList>() {
            @Override
            public void onChanged(StandingsList newStandingsList) {
                mExecutors.diskIO().execute(() -> {
                    //Actualizar drivers y constructors
                    mF1NetworkDataSource.saveDriversAndConstructors(newStandingsList);
                    Log.d("", "New values inserted in Room");
                    //Actualizar information
                    mF1NetworkDataSource.saveInformation(newStandingsList);
                    Log.d("", "New values inserted in Room");
                    //Actualizar drivers
                    mF1NetworkDataSource.saveDriverStandings(newStandingsList);
                    Log.d("", "New values inserted in Room");
                    mF1NetworkDataSource.actualizarPuntosUserLeague(newStandingsList);
                });
            }
        });

        LiveData<List<ConstructorTable>> networkDataCT = mF1NetworkDataSource.getmListDriversConstructors();
        networkDataCT.observeForever(new Observer<List<ConstructorTable>>() {
            @Override
            public void onChanged(List<ConstructorTable> newListDriversConstructors) {
                mExecutors.diskIO().execute(() -> {
                    //Meter datos en room
                    mF1NetworkDataSource.saveDriverConstructors(newListDriversConstructors);
                    Log.d("", "New values inserted in Room");
                });
            }
        });

        LiveData<List<Race>> networkDataResults = mF1NetworkDataSource.getmResults();
        networkDataResults.observeForever(new Observer<List<Race>>() {
            @Override
            public void onChanged(List<Race> newResults) {
                mExecutors.diskIO().execute(() -> {
                    //Meter datos en room
                    mF1NetworkDataSource.saveResults(newResults);
                    Log.d("", "New values inserted in Room");
                });
            }
        });

        LiveData<List<Race>> networkDataRaces = mF1NetworkDataSource.getmRaces();
        networkDataRaces.observeForever(new Observer<List<Race>>() {
            @Override
            public void onChanged(List<Race> newRaces) {
                mExecutors.diskIO().execute(() -> {
                    //Meter datos en room
                    mF1NetworkDataSource.saveRaces(newRaces);
                    Log.d("", "New values inserted in Room");
                });
            }
        });

        LiveData<ConstructorStandingsList> networkConsStand = mF1NetworkDataSource.getmConstructorStandingsList();
        networkConsStand.observeForever(new Observer<ConstructorStandingsList>() {
            @Override
            public void onChanged(ConstructorStandingsList newConstructorStandingsList) {
                mExecutors.diskIO().execute(() -> {
                    //Meter datos en room
                    mF1NetworkDataSource.saveConstructorStandings(newConstructorStandingsList);
                    Log.d("", "New values inserted in Room");
                });
            }
        });
    }

    public synchronized static F1Repository getInstance(DaoCollection daoCollection, F1NetworkDataSource nds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new F1Repository(daoCollection, nds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void doFetchData(Context context){
        Log.d("", "Cargando datos de la API");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mF1NetworkDataSource.fetchAPIData(context);
        });
    }

    /**
     * Database related operations
     **/

    public LiveData<Race> getRace() {
        return Transformations.switchMap(roundLiveData, new Function<Integer, LiveData<Race>>() {
            @Override
            public LiveData<Race> apply(Integer input) {
                return mDaoCollection.mRaceDao.get(input);
            }
        });
    }

    public LiveData<DriverComplete> getDriverComplete() {
        return Transformations.switchMap(driverIdLiveData, new Function<String, LiveData<DriverComplete>>() {
            @Override
            public LiveData<DriverComplete> apply(String input) {
                return mDaoCollection.mDriverDao.getCompleteDrivers(input);
            }
        });
    }

    public LiveData<List<Result>> getResults() {
        return Transformations.switchMap(roundLiveData, new Function<Integer, LiveData<List<Result>>>() {
            @Override
            public LiveData<List<Result>> apply(Integer input) {
                return mDaoCollection.mResultDao.getFromGP(input);
            }
        });
    }

    public LiveData<User> getUser() {
        return Transformations.switchMap(usernameLiveData, new Function<String, LiveData<User>>() {
            @Override
            public LiveData<User> apply(String input) {
                return mDaoCollection.mUserDao.get(input);
            }
        });
    }

    public LiveData<User> getUserById() {
        return Transformations.switchMap(idLiveData, new Function<Integer, LiveData<User>>() {
            @Override
            public LiveData<User> apply(Integer input) {
                return mDaoCollection.mUserDao.getById(input.intValue());
            }
        });
    }

    public LiveData<List<UserLeagues>> getFromLeaguePoints() {
        return Transformations.switchMap(idLigaLiveData, new Function<Integer, LiveData<List<UserLeagues>>>() {
            @Override
            public LiveData<List<UserLeagues>> apply(Integer input) {
                return mDaoCollection.mUserLeaguesDao.getFromLeaguePoints(input.intValue());
            }
        });
    }

    public LiveData<UserLeagues> getUserLeaguesBothIds() {
        return Transformations.switchMap(idPilotoIdLigaLiveData, new Function<IdPilotoIdLiga, LiveData<UserLeagues>>() {
            @Override
            public LiveData<UserLeagues> apply(IdPilotoIdLiga input) {
                return mDaoCollection.mUserLeaguesDao.get(input.getIdPiloto(), input.getIdLiga());
            }
        });
    }

    public LiveData<List<UserLeaguesComplete>> getFromLeague() {
        return Transformations.switchMap(idLigaLiveData, new Function<Integer, LiveData<List<UserLeaguesComplete>>>() {
            @Override
            public LiveData<List<UserLeaguesComplete>> apply(Integer input) {
                return mDaoCollection.mUserLeaguesDao.getFromLeague(input.intValue());
            }
        });
    }

    public LiveData<List<UserLeagues>> getUserLeaguesList() {
        int id_usuario = sharedPreferencesUser.getInt("id_usuario", -1);
        return mDaoCollection.mUserLeaguesDao.getFromUser(id_usuario);
    }

    public League getLeague(int id) {
        return mDaoCollection.mLeagueDao.getLeague(id);
    }

    public League getLeague(String name) {
        return mDaoCollection.mLeagueDao.get(name);
    }

    public League getLeague(String name, String password) {
        return mDaoCollection.mLeagueDao.get(name, password);
    }

    public LiveData<List<Race>> getRaces() {
        return mDaoCollection.mRaceDao.getAll();
    }

    public LiveData<List<DriverStandingComplete>> getCurrentDriverStanding() {
        int ronda = sharedPreferences.getInt("ULTIMA_RONDA_ACTUALIZADA",-1);
        return mDaoCollection.mDriverStandingDao.getFromStandingComplete(ronda);
    }

    public LiveData<List<ConstructorStandingComplete>> getCurrentConstructorStanding() {
        int ronda = sharedPreferences.getInt("ULTIMA_RONDA_ACTUALIZADA",-1);
        return mDaoCollection.mConstructorStandingDao.getFromStandingComplete(ronda);
    }

    public LiveData<List<ConstructorComplete>> getConstructorCompleteList() {
        return mDaoCollection.mConstructorDao.getAllConstructorsComplete();
    }

    public LiveData<List<Driver>> getCurrentDrivers() {
        return mDaoCollection.mDriverDao.getAll();
    }

    public LiveData<List<Constructor>> getCurrentConstructors() {
        return mDaoCollection.mConstructorDao.getAll();
    }

    public long insertUser(User user){
        return mDaoCollection.mUserDao.insert(user);
    }

    public int updateUser(User user){
        return mDaoCollection.mUserDao.update(user);
    }

    public int deleteUser(int id){
        return mDaoCollection.mUserDao.delete(id);
    }

    public void deleteUserLeagues(int id){
        mDaoCollection.mUserLeaguesDao.delete(id);
    }

    public void deleteLeague(int id){
        mDaoCollection.mLeagueDao.delete(id);
    }

    public void deleteUserLeague(int id, int userID){
        mDaoCollection.mUserLeaguesDao.delete(id, userID);
    }

    public int updateUserLeagues(UserLeagues userLeagues){
        return mDaoCollection.mUserLeaguesDao.update(userLeagues);
    }

    public void insertLeague(League league){
        mDaoCollection.mLeagueDao.insert(league);
    }

    public void insertUserLeagues(UserLeagues userLeague){
        mDaoCollection.mUserLeaguesDao.insert(userLeague);
    }


    /**
     * Checks if we have to update the repos data.
     * @return Whether a fetch is needed
     */
    private boolean isFetchNeeded(String username) {
        Long lastFetchTimeMillis = lastUpdateTimeMillisMap.get(username);
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        // TODO - Implement cache policy: When time has passed or no repos in cache
        return true;
    }

    public void setGpId(final int gpID) {
        roundLiveData.setValue(gpID);
    }

    public void setUsername(String username) {
        usernameLiveData.setValue(username);
    }

    public void setDriverId(String driverId) {
        driverIdLiveData.setValue(driverId);
    }

    public void setId(int id) {
        idLiveData.setValue(id);
    }

    public void setIdLiga(int idLiga) {
        idLigaLiveData.setValue(idLiga);
    }

    public void setIdPilotoIdLiga(IdPilotoIdLiga idPilotoIdLiga) {
        idPilotoIdLigaLiveData.setValue(idPilotoIdLiga);
    }
}
