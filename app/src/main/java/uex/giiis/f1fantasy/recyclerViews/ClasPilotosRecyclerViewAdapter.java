package uex.giiis.f1fantasy.recyclerViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.generatedPojos.DriverStanding;


public class ClasPilotosRecyclerViewAdapter extends RecyclerView.Adapter<ClasPilotosRecyclerViewAdapter.ViewHolder> {

    private List<DriverStanding> mValues;
    private Context mContext;

    public ClasPilotosRecyclerViewAdapter(List<DriverStanding> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.clasificacion_pilotos_fila, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        DriverStanding item = mValues.get(position);

        holder.mItem = item;
        holder.mPosicionView.setText(item.getPosition());
        holder.mPilotoView.setText(item.getDriver().getGivenName() + " " + item.getDriver().getFamilyName());
        holder.mConstructorView.setText(item.getConstructors().get(0).getName());
        holder.mPuntosView.setText(item.getPoints());

    }


    @Override
    public int getItemCount() {
        int resultado = 0;
        if(mValues != null){
            resultado = mValues.size();
        }
        return resultado;
    }


    public List<DriverStanding> getmValues() {
        return mValues;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mPosicionView;
        public final TextView mPilotoView;
        public final TextView mConstructorView;
        public final TextView mPuntosView;
        public DriverStanding mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPosicionView = (TextView)  view.findViewById(R.id.posicion_clas_piloto);
            mPilotoView = (TextView)  view.findViewById(R.id.piloto_constructor_clas_piloto);
            mConstructorView = (TextView)  view.findViewById(R.id.constructor_nacionalidad_clas_piloto);
            mPuntosView = (TextView)  view.findViewById(R.id.puntos_clas_piloto);


        }

    }

    public void swap(List<DriverStanding> items){
        mValues = items;
        notifyDataSetChanged();
    }

}

