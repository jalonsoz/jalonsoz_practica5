package uex.giiis.f1fantasy.recyclerViews;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.pojo.League;

import static uex.giiis.f1fantasy.ColeccionFotos.obtenerLogoConstructor;


public class EscuderiasExpandableListAdapter extends BaseExpandableListAdapter {

    private List<Constructor> constructorList;
    private Map<Constructor, List<Driver>> mapConstructorDrivers;
    private Context context;

    public interface OnListPilotosListener{
        public void onListPilotos(String id);
    }

    public OnListPilotosListener mListener;


    public EscuderiasExpandableListAdapter(List<Constructor> constructorList, Map<Constructor, List<Driver>> mapConstructorDrivers, OnListPilotosListener listener, Context context) {
        this.constructorList = constructorList;
        this.mapConstructorDrivers = mapConstructorDrivers;
        this.context = context;
        this.mListener = listener;
    }

    @Override
    public int getGroupCount() {
        return constructorList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mapConstructorDrivers.get(constructorList.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return constructorList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mapConstructorDrivers.get(constructorList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        Constructor c = (Constructor) getGroup(groupPosition);
        convertView = LayoutInflater.from(context).inflate(R.layout.escuderias_group, null);
        TextView constructorGroup = (TextView) convertView.findViewById(R.id.escuderias_group);
        constructorGroup.setText(c.getName());
        ImageView constructorLogo = (ImageView) convertView.findViewById(R.id.escuderias_logo);
        constructorLogo.setImageResource(obtenerLogoConstructor(c.getConstructorId()));
        ImageView infoEscuderia = (ImageView) convertView.findViewById(R.id.info_escuderia);

        ImageView imgExpandCollapse = (ImageView) convertView.findViewById(R.id.arrow);
        if(isExpanded){
            imgExpandCollapse.setImageResource(R.drawable.ic_arrow_up_white);
        }
        else{
            imgExpandCollapse.setImageResource(R.drawable.ic_arrow_down_white);
        }

        infoEscuderia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(c.getUrl()));
                context.startActivity(browserIntent);
            }
        });

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Driver d = (Driver) getChild(groupPosition, childPosition);
        convertView = LayoutInflater.from(context).inflate(R.layout.escuderias_child, null);
        TextView constructorChild = (TextView) convertView.findViewById(R.id.escuderias_child);
        constructorChild.setText(d.getGivenName()+" "+d.getFamilyName());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListPilotos(d.getDriverId());
                }
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void swap(List<Constructor> constructorList, Map<Constructor, List<Driver>> mapConstructorDrivers){
        this.constructorList = constructorList;
        this.mapConstructorDrivers = mapConstructorDrivers;
        notifyDataSetChanged();
    }
}

