package uex.giiis.f1fantasy.viewModels;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.ui.calendario.CalendarioFragment;
import uex.giiis.f1fantasy.repository.F1Repository;

/**
 * {@link ViewModel} for {@link CalendarioFragment}
 */
public class CalendarioViewModel extends ViewModel {

    private final F1Repository mRepository;
    private final LiveData<List<Race>> mRaces;

    public CalendarioViewModel(F1Repository repository) {
        mRepository = repository;
        mRaces = mRepository.getRaces();
    }

    public LiveData<List<Race>> getRaces() {
        return mRaces;
    }

}