package uex.giiis.f1fantasy.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.ui.RegisterActivity;

/**
 * {@link ViewModel} for {@link RegisterActivity}
 */
public class RegisterViewModel extends ViewModel {

    private final F1Repository mRepository;
    private LiveData<User> mUser;
    private String mUsername="";

    public RegisterViewModel(F1Repository repository) {
        mRepository = repository;
        mUser = mRepository.getUser();
    }

    public void setUsername (String username){
        mUsername = username;
        mRepository.setUsername(username);
    }


    public LiveData<User> getUser() {
        return mUser;
    }

    public long insertUser(User user) {
        return mRepository.insertUser(user);
    };
}