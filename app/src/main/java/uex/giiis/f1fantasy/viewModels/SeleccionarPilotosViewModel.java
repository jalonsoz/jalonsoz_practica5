package uex.giiis.f1fantasy.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.pojo.IdPilotoIdLiga;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.ui.SeleccionarPilotosActivity;

/**
 * {@link ViewModel} for {@link SeleccionarPilotosActivity}
 */
public class SeleccionarPilotosViewModel extends ViewModel {

    private final F1Repository mRepository;
    private LiveData<UserLeagues> mUserLeagues;
    private LiveData<List<Driver>> mDrivers;
    private IdPilotoIdLiga idPilotoIdLiga;

    public SeleccionarPilotosViewModel(F1Repository repository) {
        mRepository = repository;
        mDrivers = mRepository.getCurrentDrivers();
        mUserLeagues = mRepository.getUserLeaguesBothIds();
    }

    public void setIdLigaIdPiloto(int id_usuario, int idLiga) {
        idPilotoIdLiga = new IdPilotoIdLiga(id_usuario, idLiga);
        mRepository.setIdPilotoIdLiga(idPilotoIdLiga);
    }

    public LiveData<UserLeagues> getUserLeagues() {
        return mUserLeagues;
    }

    public LiveData<List<Driver>> getAllDrivers() {
        return mDrivers;
    }

    public int update(UserLeagues userLeagues) {
        return mRepository.updateUserLeagues(userLeagues);
    }
}