package uex.giiis.f1fantasy.viewModels;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import uex.giiis.f1fantasy.pojo.DriverStandingComplete;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.ui.clasificaciones.ClasificacionesPilotos;

/**
 * {@link ViewModel} for {@link ClasificacionesPilotos}
 */
public class ClasificacionesPilotosViewModel extends ViewModel {

    private final F1Repository mRepository;
    private final LiveData<List<DriverStandingComplete>> mDriverStandingComplete;

    public ClasificacionesPilotosViewModel(F1Repository repository) {
        mRepository = repository;
        mDriverStandingComplete = mRepository.getCurrentDriverStanding();
    }

    public LiveData<List<DriverStandingComplete>> getDriverStandingComplete() {
        return mDriverStandingComplete;
    }


}
