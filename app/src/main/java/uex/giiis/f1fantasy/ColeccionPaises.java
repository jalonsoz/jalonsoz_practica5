package uex.giiis.f1fantasy;

import android.util.Log;

public class ColeccionPaises {

    public static String [] paisId = {"Argentina", "Australia", "Austria", "Azerbaijan", "Bahrain",
            "Belgium", "Brazil", "Canada", "China", "Colombia", "Europe", "Finland", "France",
            "Germany", "Hungary", "Indonesia", "Italy", "Japan", "Mexico", "Monaco", "Netherlands",
            "Portugal", "Russia", "San Marino", "Singapur", "Spain", "Sweden", "Switzerland",
            "Turkey", "UAE", "UK", "USA"};
    public static int [] resource = {R.mipmap.argentina, R.mipmap.australia, R.mipmap.austria,
            R.mipmap.azerbaijan, R.mipmap.bahrain, R.mipmap.belgium, R.mipmap.brazil,
            R.mipmap.canada, R.mipmap.china, R.mipmap.colombia, R.mipmap.europe,
            R.mipmap.finland, R.mipmap.france, R.mipmap.germany, R.mipmap.hungary,
            R.mipmap.indonesia, R.mipmap.italy, R.mipmap.japan, R.mipmap.mexico,
            R.mipmap.monaco, R.mipmap.netherlands, R.mipmap.portugal, R.mipmap.russia,
            R.mipmap.san_marino, R.mipmap.singapur, R.mipmap.spain, R.mipmap.sweden,
            R.mipmap.switzerland, R.mipmap.turkey, R.mipmap.uae, R.mipmap.uk,
            R.mipmap.usa};

    public static String [] circuitId = {"albert_park", "red_bull_ring", "bahrain",
            "spa", "nurburgring", "hungaroring", "monza", "mugello", "imola",
            "hermanos_rodriguez", "portimao", "sochi", "catalunya", "istanbul", "yas_marina", "silverstone"};
    public static int [] resourceCircuit = {R.mipmap.circuit_albert_park, R.mipmap.circuit_austria,
            R.mipmap.circuit_bahrain, R.mipmap.circuit_spa, R.mipmap.circuit_nurburgring,
            R.mipmap.circuit_hungaroring, R.mipmap.circuit_monza, R.mipmap.circuit_mugello,
            R.mipmap.circuit_imola, R.mipmap.circuit_hermanos_rodriguez, R.mipmap.circuit_algarve,
            R.mipmap.circuit_sochi, R.mipmap.circuit_catalunya, R.mipmap.circuit_istanbul_park,
            R.mipmap.circuit_yas_marina, R.mipmap.circuit_silverstone};

    public static int obtenerPais (String pais) {
        boolean salir = false;
        int returnResource = -1;
        for (int i = 0; i < paisId.length && !salir; i++) {
            if (paisId[i].equals(pais)) {
                returnResource = resource[i];
                salir = true;
            }
        }
        if (returnResource == -1) {
            returnResource = R.mipmap.unknown;
        }
        return returnResource;
    }

    public static int obtenerCircuito (String circuito) {
        boolean salir = false;
        int returnResource = -1;
        for (int i = 0; i < circuitId.length && !salir; i++) {
            if (circuitId[i].equals(circuito)) {
                returnResource = resourceCircuit[i];
                salir = true;
            }
        }
        if (returnResource == -1) {
            returnResource = R.mipmap.circuit_unknown;
        }
        return returnResource;
    }
}
