package uex.giiis.f1fantasy.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.pojo.ConstructorComplete;

@Dao
public interface ConstructorDao {

    @Query("SELECT * FROM constructor")
    public LiveData<List<Constructor>> getAll();

    @Transaction
    @Query("SELECT * FROM constructor")
    public LiveData<List<ConstructorComplete>> getAllConstructorsComplete();

    @Query("SELECT * FROM constructor WHERE constructorId = :constructorId")
    public Constructor get(String constructorId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll (List<Constructor> constructors);

    @Update
    public int updateAll (List<Constructor> constructors);

    @Query("DELETE FROM constructor")
    void deleteAll();
}
