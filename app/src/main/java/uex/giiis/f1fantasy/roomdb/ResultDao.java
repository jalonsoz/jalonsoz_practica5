package uex.giiis.f1fantasy.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Result;

@Dao
public interface ResultDao {

    @Query("SELECT * FROM result")
    public List<Result> getAll();

    @Query("SELECT * FROM result WHERE round = :idGP")
    public LiveData<List<Result>> getFromGP(int idGP);

    @Query("SELECT * FROM result WHERE round = :idGP")
    public List<Result> getFromGP2(int idGP);

    @Query("SELECT * FROM result WHERE idDriver = :idDriver")
    public List<Result> getFromDriver(String idDriver);

    @Query("SELECT * FROM Result WHERE round = :idGP AND idDriver = :idDriver")
    public Result get(int idGP, String idDriver);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert (Result gpResult);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll (List<Result> gpResults);

    @Update
    public int update (Result gpResult);

    @Delete
    public int delete (Result gpResult);
}
