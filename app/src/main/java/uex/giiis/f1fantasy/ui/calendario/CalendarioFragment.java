package uex.giiis.f1fantasy.ui.calendario;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.pojo.DriverStandingComplete;
import uex.giiis.f1fantasy.recyclerViews.CalendarioRecyclerViewAdapter;
import uex.giiis.f1fantasy.recyclerViews.LigasRecyclerViewAdapter;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.CalendarioViewModel;
import uex.giiis.f1fantasy.viewModels.ClasificacionesPilotosViewModel;

public class CalendarioFragment extends Fragment {

    private CalendarioViewModel calendarioViewModel;
    private CalendarioRecyclerViewAdapter adapterProximos;
    private CalendarioRecyclerViewAdapter adapterFinalizados;
    private List<Race> racesProximos;
    private List<Race> racesFinalizados;
    private LigasRecyclerViewAdapter.OnListInteractionListener mCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        racesProximos = new ArrayList<>();
        racesFinalizados = new ArrayList<>();
        //racesDatabase();
        adapterProximos = new CalendarioRecyclerViewAdapter(racesProximos, mCallback, getContext());
        adapterFinalizados = new CalendarioRecyclerViewAdapter(racesFinalizados, mCallback, getContext());
        racesDatabase();

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_calendario, container, false);

        // Proximos GPS
        RecyclerView recyclerViewProx;
        RecyclerView.LayoutManager layoutManagerProx;

        recyclerViewProx = (RecyclerView) view.findViewById(R.id.list_proximos);
        recyclerViewProx.setHasFixedSize(true);

        layoutManagerProx = new LinearLayoutManager(view.getContext());
        recyclerViewProx.setLayoutManager(layoutManagerProx);

        recyclerViewProx.setAdapter(adapterProximos);

        // GPS Finalizados
        RecyclerView recyclerViewFin;
        RecyclerView.LayoutManager layoutManagerFin;

        recyclerViewFin = (RecyclerView) view.findViewById(R.id.list_finalizados);
        recyclerViewFin.setHasFixedSize(true);

        layoutManagerFin = new LinearLayoutManager(view.getContext());
        recyclerViewFin.setLayoutManager(layoutManagerFin);

        recyclerViewFin.setAdapter(adapterFinalizados);

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LigasRecyclerViewAdapter.OnListInteractionListener) {
            mCallback = (LigasRecyclerViewAdapter.OnListInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListCalendarioListener");
        }
    }

    private void actualizarRecyclerView(List<Race> updateProximos, List<Race> updateFinalizados) {
        adapterFinalizados.swap(updateFinalizados);
        adapterProximos.swap(updateProximos);
    }

    public void racesDatabase () {

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        CalendarioViewModel mViewModel = new ViewModelProvider(this, appContainer.calendarioFactory)
                .get(CalendarioViewModel.class);
        mViewModel.getRaces().observe(this, new Observer<List<Race>>() {
            @Override
            public void onChanged(List<Race> races) {
                racesFinalizados.clear();
                racesProximos.clear();

                SharedPreferences sharedPreferences = getContext().getSharedPreferences("ultimaModificacion", getContext().MODE_PRIVATE);
                int ronda = sharedPreferences.getInt("ULTIMA_RONDA_ACTUALIZADA",-1);

                for (Race t : races) {
                    if (Integer.parseInt(t.getRound()) > ronda) {
                        racesProximos.add(t);
                    } else {
                        racesFinalizados.add(t);
                    }

                }

                actualizarRecyclerView(racesProximos, racesFinalizados);

            }
        });
    }
}