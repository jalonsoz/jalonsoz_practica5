package uex.giiis.f1fantasy.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Toast;

import java.util.Calendar;

import java.util.ArrayList;
import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.pojo.DriverComplete;
import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.recyclerViews.DetallesLigaRecyclerViewAdapter;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.DetallesLigaViewModel;
import uex.giiis.f1fantasy.viewModels.DetallesPilotoViewModel;

public class DetallesLigaActivity extends AppCompatActivity {

    private SharedPreferences sharedPref;

    private DetallesLigaRecyclerViewAdapter adapter;
    private List<UserLeagues> userLeagues;
    private int idLiga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clasificacion_liga_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        Bundle bundle = getIntent().getExtras();
        idLiga = Integer.parseInt(bundle.getString("ID_LIGA"));
        userLeagues = new ArrayList<UserLeagues>();
        adapter = new DetallesLigaRecyclerViewAdapter(userLeagues, this);

        RecyclerView recyclerView;
        RecyclerView.LayoutManager layoutManager;

        recyclerView = (RecyclerView) findViewById(R.id.list_usuarios_clasificacion_liga);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        usuariosLigaDatabase();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void actualizarRecyclerView(List<UserLeagues> updateUserLeagues) {
        adapter.swap(updateUserLeagues);
    }

    public void seleccionarPilotos(View v){

        //Solo hacer el intent si no es viernes, sabado o domingo
        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

        if(day != Calendar.FRIDAY && day != Calendar.SATURDAY && day != Calendar.SUNDAY){
            Intent intent = new Intent(this, SeleccionarPilotosActivity.class);
            intent.putExtra("ID_LIGA", Integer.toString(idLiga));
            startActivity(intent);
        }else{
            Toast toast = Toast.makeText(this, "Sólo puedes acceder a esta funcionalidad de lunes a jueves",Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void verPilotosElegidos(View v){

        //Solo hacer el intent si no es viernes, sabado o domingo
        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

        if(day != Calendar.MONDAY && day != Calendar.TUESDAY && day != Calendar.WEDNESDAY && day != Calendar.THURSDAY){
            Intent intent = new Intent(this, VerPilotosElegidosActivity.class);
            intent.putExtra("ID_LIGA", Integer.toString(idLiga));
            startActivity(intent);
        }else{
            Toast toast = Toast.makeText(this, "Sólo puedes acceder a esta funcionalidad de viernes a domingo",Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void cerrarSesion (MenuItem item) {
        sharedPref = DetallesLigaActivity.this.getSharedPreferences("usuario", DetallesLigaActivity.this.MODE_PRIVATE);

        int id_usuario = sharedPref.getInt("id_usuario", -1);

        if (id_usuario != -1) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("id_usuario", -1);
            editor.commit();

            Intent intent = new Intent (this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public void editarPerfil (MenuItem item) {
        Intent intent = new Intent(this, PerfilActivity.class);
        startActivity(intent);
    }

    public void salirLiga (View view) {
        AlertDialog diaBox = AskOption();
        diaBox.show();


    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                // set message, title, and icon
                .setTitle("Abandonar Liga")
                .setMessage("¿Estás seguro? Si eres el propietario la liga se borrará")
                .setIcon(R.drawable.ic_baseline_delete_24)

                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        sharedPref = DetallesLigaActivity.this.getSharedPreferences("usuario", DetallesLigaActivity.this.MODE_PRIVATE);

                        int id_usuario = sharedPref.getInt("id_usuario", -1);

                        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
                        DetallesLigaViewModel mViewModel = new ViewModelProvider(DetallesLigaActivity.this, appContainer.detallesLigaFactory)
                                .get(DetallesLigaViewModel.class);

                        AppExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {

                                League league = mViewModel.getLeague(idLiga);

                                int owner = league.getOwner();

                                if (id_usuario == owner) {
                                    mViewModel.deleteLeague(idLiga);
                                } else {
                                    mViewModel.deleteUserLeague(idLiga, id_usuario);
                                }

                                finish();
                                dialog.dismiss();
                            }
                        });
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();

        return myQuittingDialogBox;
    }

    public void usuariosLigaDatabase () {

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        DetallesLigaViewModel mViewModel = new ViewModelProvider(DetallesLigaActivity.this, appContainer.detallesLigaFactory)
                .get(DetallesLigaViewModel.class);
        mViewModel.setIdLiga(idLiga);
        mViewModel.getFromLeaguePoints().observe(DetallesLigaActivity.this, new Observer<List<UserLeagues>>() {
            @Override
            public void onChanged(List<UserLeagues> userLeagueList) {
                userLeagues = userLeagueList;
                actualizarRecyclerView(userLeagues);
            }
        });
    }

}