package uex.giiis.f1fantasy.UC02tests;

import uex.giiis.f1fantasy.pojo.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing%22%3ETesting documentation</a>
 */
public class LeagueTest {

    private League league;

    @Test
    public void leagueShouldBeCreated() {
        league = new League();
        league.setId(1);
        league.setName("Liga de Prueba");
        league.setPassword("1234");
        league.setOwner(142);
        assertEquals(1, league.getId());
        assertEquals("Liga de Prueba", league.getName());
        assertEquals("1234", league.getPassword());
        assertEquals(142, league.getOwner());
    }
}
