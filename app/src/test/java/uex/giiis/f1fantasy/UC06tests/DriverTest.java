package uex.giiis.f1fantasy.UC06tests;

import uex.giiis.f1fantasy.generatedPojos.Driver;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DriverTest {

    private Driver driver;

    @Test
    public void driverShouldBeCreated() {
        driver = new Driver();
        driver.setDriverId("sainz");
        driver.setPermanentNumber("55");
        driver.setCode("SAI");
        driver.setDateOfBirth("1994-09-01");
        driver.setGivenName("Carlos");
        driver.setFamilyName("Sainz");
        driver.setNationality("Spanish");
        driver.setUrl("http://en.wikipedia.org/wiki/Carlos_Sainz_Jr.");
        driver.setIdConstructor("mclaren");

        assertEquals("sainz", driver.getDriverId());
        assertEquals("55", driver.getPermanentNumber());
        assertEquals("SAI", driver.getCode());
        assertEquals("1994-09-01", driver.getDateOfBirth());
        assertEquals("Carlos", driver.getGivenName());
        assertEquals("Sainz", driver.getFamilyName());
        assertEquals("Spanish", driver.getNationality());
        assertEquals("http://en.wikipedia.org/wiki/Carlos_Sainz_Jr.", driver.getUrl());
        assertEquals("mclaren", driver.getIdConstructor());
    }
}